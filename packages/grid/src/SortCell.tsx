import { blue } from "@material-ui/core/colors"
import { ArrowDropDown, ArrowDropUp } from "@material-ui/icons"
import { makeStyles } from "@material-ui/styles"
import React from "react"

const useStyles = makeStyles(() => ({
	headerCell: {
		fontWeight: "bold",
		cursor: "pointer",
		display: "flex",

		"&:hover": {
			color: blue[800],
		},
	},
}))

const SortCell = ({ model, prop, params, fetchDataGrid, setUpdateCacheState }) => {
	const classes = useStyles()

	const data = model[prop]

	const { orderDirection } = data

	const onClick = () => {
		/* ### Clear order driections ### */

		const keys = Object.keys(model)

		keys.forEach(key => {
			if (key !== prop) {
				model[key].orderDirection = ""
			}
		})

		/* ### Clear order driections ### */

		if (orderDirection === "") {
			data.orderDirection = "desc"
		} else if (orderDirection === "desc") {
			data.orderDirection = "asc"
		} else if (orderDirection === "asc") {
			data.orderDirection = "desc"
		}

		fetchDataGrid({ ...params, orderBy: prop, orderDirection: data.orderDirection }, () =>
			setUpdateCacheState(true),
		)
	}

	const ascIcon = orderDirection === "asc" && <ArrowDropDown />
	const descIcon = orderDirection === "desc" && <ArrowDropUp />

	return (
		<span className={classes.headerCell} onClick={onClick}>
			{ascIcon} {descIcon} {data.title}
		</span>
	)
}

export default SortCell
