import { green, red } from "@material-ui/core/colors"
import React from "react"

const greenColor = { color: green[600] }
const redColor = { color: red[600] }

const NormilizeCell = ({ model, row, prop, params, fetchDataGrid, classes, setUpdateCacheState }) => {
	const getType = model[prop].type

	if (Object.keys(row).length === 0) {
		// header row
		return (
			<SortCell
				classes={classes}
				fetchDataGrid={fetchDataGrid}
				model={model}
				params={params}
				prop={prop}
				setUpdateCacheState={setUpdateCacheState}
			/>
		)
	}

	const value = row[prop] || null

	switch (getType) {
		case String:
			return value
		case Number:
			return value
		case Boolean:
			return value ? <b style={greenColor}>True</b> : <b style={redColor}>False</b>

		default:
			return null
	}
}

export default NormilizeCell
