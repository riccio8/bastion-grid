import { CircularProgress } from "@material-ui/core"
import React from "react"

const Loading = () => {
	return (
		<div
			style={{
				position: "relative",
				float: "left",
				top: "50%",
				left: "50%",
				transform: "translate(-50%, -50%)",
			}}
		>
			<CircularProgress />
		</div>
	)
}

export default Loading
