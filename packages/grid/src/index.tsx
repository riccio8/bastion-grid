import { jssPreset, makeStyles, StylesProvider } from "@material-ui/styles"
import Loading from "components/Grid/Loading"
import NormilizeCell from "components/Grid/NormilizeCell"
import Pagination from "components/Grid/Pagination"
import { create } from "jss"
import React, { useEffect, useRef, useState } from "react"
import { AutoSizer, CellMeasurer, CellMeasurerCache, MultiGrid } from "react-virtualized"

const cache = new CellMeasurerCache({
	fixedHeight: true,
	defaultHeight: 30,
	keyMapper: (rowIndex, columnIndex) => `${rowIndex}${columnIndex}`,
})

const useStyles = makeStyles(() => ({
	bottomToolbar: {
		width: "100%",
		height: 60,
		backgroundColor: "#fff",
	},
}))

const defaultStyle = {
	fontSize: ".75rem",
	whiteSpace: "nowrap",
	display: "flex",
	alignItems: "center",
	padding: "0 1rem",
	cursor: "default",
}

const Grid = ({
	actions: Actions,
	columnCount,
	fetchDataGrid,
	isUpdateCache,
	items,
	loading,
	model,
	params,
	setParams,
	setUpdateCacheState,
	totalRowCount,
}) => {
	const classes = useStyles()

	const ref = useRef()

	const [selections, setSelections] = useState({
		rowIndex: 0,
		columnIndex: 0,
	})

	const modelKeys = Object.keys(model)

	useEffect(() => {
		if (isUpdateCache) {
			cache.clearAll()
			setUpdateCacheState(false)
		}
	}, [isUpdateCache, setUpdateCacheState])

	const cellRenderer = ({ columnIndex, key, rowIndex, style, parent }) => {
		const prop = modelKeys[columnIndex]

		const row = items[rowIndex]

		const isSelectedPath = rowIndex === selections.rowIndex || columnIndex === selections.columnIndex
		const isSelectedCurrentCell = rowIndex === selections.rowIndex && columnIndex === selections.columnIndex

		let backgroundColor = "#fff"

		if (isSelectedPath) {
			backgroundColor = "#eee"
		}

		if (isSelectedCurrentCell) {
			backgroundColor = "#ddd"
		}

		const makeStyle = {
			...style,
			...defaultStyle,
			backgroundColor,
		}

		const onClick = () => {
			setSelections({ rowIndex, columnIndex })
			ref.current.forceUpdateGrids()
		}

		if (columnIndex === columnCount && rowIndex === 0) {
			return null
		}

		if (columnIndex === columnCount && rowIndex !== 0) {
			return (
				<CellMeasurer key={key} cache={cache} columnIndex={columnIndex} parent={parent} rowIndex={rowIndex}>
					<div style={makeStyle}>
						<Actions row={row} />
					</div>
				</CellMeasurer>
			)
		}

		return (
			<CellMeasurer key={key} cache={cache} columnIndex={columnIndex} parent={parent} rowIndex={rowIndex}>
				<div style={makeStyle} onClick={onClick}>
					<NormilizeCell
						classes={classes}
						fetchDataGrid={fetchDataGrid}
						model={model}
						params={params}
						prop={prop}
						row={row}
						setUpdateCacheState={setUpdateCacheState}
					/>
				</div>
			</CellMeasurer>
		)
	}

	const isShowGrid = !loading && items.length > 0

	return (
		<div style={{ display: "flex", flexDirection: "column", flex: "1 1 auto" }}>
			<div style={{ flex: "1 1 auto" }}>
				{isShowGrid && (
					<AutoSizer>
						{({ width, height }) => (
							<MultiGrid
								cellRenderer={cellRenderer}
								columnCount={columnCount + 1}
								columnWidth={cache.columnWidth}
								deferredMeasurementCache={cache}
								enableFixedRowScroll
								fixedColumnCount={1}
								fixedRowCount={1}
								height={height}
								hideBottomRightGridScrollbar
								hideTopRightGridScrollbar
								overscanColumnCount={0}
								overscanRowCount={0}
								ref={ref}
								rowCount={items.length}
								rowHeight={cache.rowHeight}
								scrollToColumn={0}
								scrollToRow={0}
								width={width}
							/>
						)}
					</AutoSizer>
				)}
				{loading && <Loading />}
			</div>

			<Pagination classes={classes} setParams={setParams} params={params} totalRowCount={totalRowCount} />
		</div>
	)
}

export default Grid
